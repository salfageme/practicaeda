# PRACTICA 1 - ESTRUCTURAS DE DATOS Y ALGORITMOS - 2-G-ING.INFORMATICA
# SAMUEL ALFAGEME SAINZ (L1)
import sys

class EntradaParser:
	def __init__(self,path):
		with open(path,"r") as f:
			self.instructions = f.readlines()
		self.nRows , self.mColumns = int(self.instructions[0].replace("\n","")), int(self.instructions[1].replace("\n",""))
		self.instructions = [i.replace("\n","") for i  in self.instructions[2:]]
		self.lenIns = [len(l) for l in self.instructions]
		self.lenInsMax , self.lenInsMin = max(self.lenIns), min(self.lenIns)x
		# UGLY CODE:
		if self.nRows != len(self.instructions) or self.mColumns!=self.lenInsMin or self.mColumns!=self.lenInsMax:
			raise Exception("El formato del fichero no es correcto")
	def info(self):
		print "Filas: " + str(self.nRows)
		print "Columnas: " + str(self.mColumns)
	def printPattern(self):
		for a in self.instructions:
			print a
			a = [lambda e: e=="X" for e in a]
			print a

if __name__ == "__main__" :
	am = EntradaParser(sys.argv[1])
	am.info()
	am.printPattern()
	