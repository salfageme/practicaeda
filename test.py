# PRACTICA 1 - ESTRUCTURAS DE DATOS Y ALGORITMOS - 2-G-ING.INFORMATICA
# SAMUEL ALFAGEME SAINZ (L1)
# test.py - MODULO DE PRUEBA PARA LA PRACTICA 1
from practicaEDA import EntradaParser
import csv, datetime
def invTrans(matr):
		# TODO
		t = lambda x: "X" if x else "."
		return ["".join([t(m) for m in a]) for a in matr]
def printMat(matr):
	for line in matr:
		print line
filePth = raw_input("Especificar la ruta del fichero: ")
numIt = input("Numero de iteracciones sobre la matriz: ")
print ""
am = EntradaParser.EntradaParser(filePth)
am.printPattern()
from practicaEDA import LifeGameP
foo = am.makePattern()
jorl = LifeGameP.LifeGame(foo,numIt)
f = open(filePth.split(".")[0]+".csv","wb")
wrt = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
wrt.writerow(["ITERACION","TIEMPO"])
bar = datetime.datetime.now()
timesList = []
for i  in range(numIt):
	jorl.expandUniverse()
	jorl.makeIter()
	jorl.minimumUniverse()
	baz = datetime.datetime.now()
	m = baz-bar
	timesList.append(m)
for it,timeStamp in enumerate(timesList):
	time = timeStamp.seconds*1000000+timeStamp.microseconds
	timesList[it] = time
	wrt.writerow([it,time]) 
print ""
print str(jorl.alive) + " celdas vivas"
print str(timesList[-1]/float(1000000)) + " segundos"
print ""
print "RESULTADO: "
print ""
f.close()
pin = jorl.getUniverse()
pin = invTrans(pin)
printMat(pin)