// PR1 - ESTRUCTURAS DE DATOS Y ALGORITMOS - G.ING.INFORMATICA (UVa)
// Autores (L1):
// 	- Daniel Barba Gutierrez 
//	- Samuel Alfageme Sainz
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class ConwaysLife {
	
	public static void main(String[] args){
		
		boolean[][] matrix = null;
		int iter;
		int vivas;
		String fileName;
		String fileResult = null;
		Scanner in = new Scanner(System.in);
		long time = 0;

		// Peticion de datos de entrada: 
		fileName = askFileName(in);
		iter = askIterNum(in);
		matrix = readFile(fileName);

		// Generacion del archivo .csv que contendra el tiempo frente a las iteraciones
		// Se creara un archivo en el mismo directorio que el patron, con su mismo nombre.
		PrintWriter writer;
		try {
			writer = new PrintWriter(new FileWriter(fileName.split("\\.")[0] + ".csv")); 
			writer.println("Iteraciones,Tiempo");
		} catch (IOException e) {
			System.out.println("El fichero de valores no se pudo crear.");
			return;
		}
		
		// Ejecucion de la rutina del programa, en la variable time se acumulara el tiempo 
		// de cada iteracion, de interes para medir la eficiencia del algoritmo.
		for (int c=0; c < iter; c++){
			long t1 = System.nanoTime();
			matrix = expandWorld(matrix);
			matrix = lifeStep(matrix);
			matrix = reduceUniverse(matrix);
			// En el caso en el que el universo este completamente vacio, el programa terminara
			if (matrix==null) {
				System.out.println("El universo se ha destruido tras " + (c+1) + " iteraciones.");
				return;	
			}
			long delta = (System.nanoTime()-t1)/1000;
			time += delta;
			writer.println(c+1 + "," + time/1000.0);
		}
		
		writer.flush();
		writer.close();
				
		vivas = checkAlive(matrix);

		// Proceso de muestra de resultados:
		System.out.print("Introduzca la ruta del fichero destino: ");
		in = new Scanner(System.in);
		fileResult = in.nextLine();
		if(!fileResult.equals("")) writeFile(matrix, fileResult);
		else fileResult=null;
		in.close();
		writeInfo(matrix, vivas, fileName, fileResult, iter, time);
	}
	
	public static boolean[][] lifeStep(boolean[][] matOrig){
		// Aplica las reglas del juego de la vida sobre la matriz booleana 
		// del estado n que se le pase como argumento y devuelve la matriz 
		// correspondiente al estado n+1 del automata
		int filas = matOrig.length;
		int columnas = matOrig[0].length;
		int vivas;
		boolean[][] matNew = new boolean[filas][columnas];
		
		for (int i=0; i<filas; i++){
			for (int j=0; j<columnas; j++){
				vivas = 0;
				for (int i1=-1; i1<2; i1++){
					for (int j1=-1; j1<2; j1++){
						if ((i+i1>=0 && i+i1<filas) && (j+j1>=0 && j+j1<columnas)){
							if (matOrig[i+i1][j+j1]) vivas++;
						}
					}
				}
				if (matOrig[i][j]){
					vivas--;
					if(!(vivas==2||vivas==3)) matNew[i][j] = false;
					else matNew[i][j] = matOrig[i][j];
				}else{
					if (vivas==3) matNew[i][j] = true;
					else matNew[i][j] = matOrig[i][j];
				}
			}
		}
		return matNew;
	}
	
	public static int checkAlive(boolean[][] matOrig){
		// Devuelve el numero de celulas vivas en el universo matOrig.
		int filas = matOrig.length;
		int columnas = matOrig[0].length;
		int vivas = 0;
		
		for (int i=0; i<filas; i++){
			for (int j=0; j<columnas; j++){
				if (matOrig[i][j]) vivas++;
			}
		}
		return vivas;
	}
	
	public static boolean[][] expandWorld(boolean[][] matOrig){
		// Realiza el paso de ampliación de la matriz para simular el universo infinito 
		// paso a paso.
		int filas = matOrig.length+2;
		int columnas = matOrig[0].length+2;
		boolean[][] matNew = new boolean[filas][columnas];
				
		for (int j=0; j<columnas; j++){
			matNew[0][j]=false;
			matNew[filas-1][j]=false;
		}
		for (int i=0; i<filas; i++){
			matNew[i][0]=false;
			matNew[i][columnas-1]=false;
		}
		for (int i=1; i<filas-1; i++){
			for (int j=1; j<columnas-1; j++){
				matNew[i][j]=matOrig[i-1][j-1];
			}
		}
		
		return matNew;
	}
	public static boolean functOR(boolean [] vector){
		// Funcion aux. que dada un array de valores booleanos, devuelve el resultado de
		// aplicar una operacion OR a todos sus elementos.
		boolean orOp = vector[0];
		for(int i=1;i<vector.length;i++){
			orOp = (vector[i]|orOp);
		}
		return orOp;
	}
	
	public static boolean[][] reduceUniverse(boolean[][] matOrig){
		// Reduce el universo de manera que las filas/columnas mas externas no esten vacias
		int filas = matOrig.length;
		int columnas = matOrig[0].length;
		boolean[][] matNew = null;
		int a,b,c,d; // Izq, Arriba, Derecha, Abajo
		a=b=c=d=0;
		boolean paraL = false;
		boolean paraR = false;
		
		while(!functOR(matOrig[b])) {
			b++; // Calcula el indice de la primera fila no vacia por arriba
			if(b==filas-1){
				// En caso de que el indice de la fila sea el ultimo, no quedara universo
				return null;
			}
		}
		while(!functOR(matOrig[filas-1-d])) d++; // Idem por abajo
		
		while(!paraL){
			// Calcular el indice de la primera columna no vacia por la Izq.
			boolean orOp = matOrig[0][a];
			for(int i=1;i<filas;i++){
				orOp = (matOrig[i][a]|orOp);
			}
			paraL = orOp;
			if(!paraL) a++;
		}
		while(!paraR){
			// Idem para la derecha
			boolean orOpR = matOrig[0][columnas-1-c];
			for(int i=1;i<filas;i++){
				orOpR = (matOrig[i][columnas-1-c]|orOpR);
			}
			paraR = orOpR;
			if(!paraR) c++;
		}
		int newFilas = filas-b-d;
		int newColumnas = columnas-a-c;
		matNew = new boolean[newFilas][newColumnas];
		for (int i=0; i<newFilas; i++){
			for (int j=0; j<newColumnas; j++){
				matNew[i][j] = matOrig[i+b][j+a];
			}
		}
		matOrig = matNew;
		return matNew;
	}
	
	public static String askFileName(Scanner in){
		// Peticion y recogida de la ruta del fichero del estado inicial del automata.
		String s = null;
		boolean fileNameCorrect = false;
				
		while (!fileNameCorrect){
			System.out.print("Insertar la ruta del fichero patron: ");
			s = in.nextLine();
			try {
				Scanner test = new Scanner(new FileReader(s));
				test.close();
				fileNameCorrect = true;
			} catch (FileNotFoundException e) {
				System.out.print("Error abriendo fichero; ");
			}
		}
		return s;
	}
	
	public static int askIterNum(Scanner in){
		// Peticion del numero de iteraciones al usuario, numero entero positivo.
		int i=0; 
		boolean iterCorrect = false;
		while (!iterCorrect){
			System.out.print("Especificar numero de iteraciones: ");
			while(!in.hasNextInt()) {
				in.next();
				System.out.print("Introduzca un numero entero: ");
			}
			i = in.nextInt();
			if (i>0) iterCorrect = true;
		}
		return i;
	}
	
	public static boolean[][] readFile(String fileName){
		// Lee el fichero y lo transforma en un Array booleano de 2 dimensiones
		// PRECON: que las dimensiones especificadas en la primera linea sean correctas
		Scanner reader;
		boolean fileOpen = false;
		boolean [][] m = null; 
		while (!fileOpen){
			try {
				reader = new Scanner(new FileReader(fileName));
				fileOpen = true;
				int filas = reader.nextInt();
				int columnas = reader.nextInt();
				m = new boolean[filas][columnas];
				for (int i=0; i<filas; i++){
					String fila = reader.next();
					for (int j=0; j<columnas; j++){
						if (fila.charAt(j)=='X') m[i][j] = true;
						else m[i][j] = false;
					}
				}
				reader.close();
			} catch (FileNotFoundException e) {
				System.out.println("No se ha encontrado el fichero.");
			}
		}
		return m;
	}
	
	public static void writeFile(boolean[][] matrix, String fileName){
		// Escribe en la ruta fileName el resultado de transformar la matriz al formato inicial
		// Filas, Columnas, Matriz.
		PrintWriter writer;
		try {
			writer = new PrintWriter(new FileWriter(fileName));
		} catch (IOException e) {
			System.out.println("El fichero no se pudo guardar");
			return;
		}
			writer.println(matrix.length);
			writer.println(matrix[0].length);
			for (int i=0; i<matrix.length; i++){
				for (int j=0; j<matrix[0].length; j++){
					if (matrix[i][j]) writer.print('X');
					else writer.print('.');
				}
				writer.println();
			}
			writer.flush();
			writer.close();
	}
	
	public static void writeInfo(boolean[][] matrix, int vivas, String fileOpen, String fileWritten, int iter, long time){
		// Impresion de retroalimentacion sobre la ejecucion del programa.
		System.out.println("Fichero origen: " + fileOpen);
		System.out.println("Numero de iteraciones: " + iter);
		System.out.println("Dimensiones finales: " + matrix.length + "x" + matrix[0].length);
		System.out.println("Celdas vivas: " + vivas);
		System.out.println("Tiempo total empleado: " + time/1000.0 + " ms");
		if(fileWritten!=null) System.out.println("Fichero destino: " + fileWritten);
		
	}
}