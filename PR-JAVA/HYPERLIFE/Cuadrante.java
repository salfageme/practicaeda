/**
 * PRACTICA 2 - ESTRUCTURAS DE DATOS Y ALGORITMOS - Curso 2013/2014
 * 
 * @author Daniel Barba Gutierrez (L1)
 * @author Samuel Alfageme Sainz (L1)
 */
import java.math.BigInteger;

/**
 * Clase que implementa el objeto <tt>Cuadrante</tt> que describira los estados del automata 
 */
public class Cuadrante{
	
	private final int NIVEL;				// Nivel del nodo
	private final BigInteger VIVAS;			// Numero de celdas vivas
	private final Cuadrante NW, NE, SW, SE;	// Subcuadrantes del nodo
	private final int HASHCODE;				// Codigo Hash del objeto
	private Cuadrante res = null;			// Referencia al cuadrante 
	// Tabla de dispersion cerrada en la que se almacenaran los diferentes Cuadrantes
	private static HashTable almacen = new HashTable(256, 0.8);
	// Cuadrantes basicos equivalentes a las celulas del automata
	private static final Cuadrante MUERTO = new Cuadrante(0);
	private static final Cuadrante VIVO = new Cuadrante(1);

	/**
	 * Constructor de <tt>Cuadrante</tt> de nivel 0
	 * 
	 * @param val: 0 o 1 dependiendo del estado de la celda que representa
	 */
	private Cuadrante(int val){
		this.NIVEL = 0;
		this.NW = null;
		this.NE = null;
		this.SW = null;
		this.SE = null;
		this.VIVAS = BigInteger.valueOf(val);
		this.HASHCODE = this.hashCode();
	}
	/**
	 * Constructor de <tt>Cuadrante</tt> de niveles superiores, a partir de las 4 referencias a sus subcuadrantes 
	 * 
	 * @param nwl, nel, swl, sel:  referencias a los 4 subcuadrantes inferiores
	 */
	private Cuadrante(Cuadrante nwl, Cuadrante nel, Cuadrante swl, Cuadrante sel){
		this.NIVEL = nwl.getNivel()+1;
		this.NW = nwl;
		this.NE = nel;
		this.SW = swl;
		this.SE = sel;
		this.VIVAS = BigInteger.ZERO.add(nwl.getVivas()).add(nel.getVivas()).add(swl.getVivas()).add(sel.getVivas());
		this.HASHCODE = this.hashCode();
		//Si tiene 0 celdas vivas... la siguiente iteracion tambien, �podemos ahorrarnos operaciones?
	}
	
	/**
	 * Creador de cuadrantes de nivel 0; al ser los cuadrantes basicos, se trata de variables estaticas en la clase <tt>Cuadrante</tt>
	 * 
	 * @param val: valor del cuadrante a crear.
	 * @return el cuadrante asociado al valor del parametro <tt>val</tt>
	 */
	public static Cuadrante crear(int val){
		if (val == 0){
			return MUERTO;
		}
		return VIVO;
	}
	
	/**
	 * Creador de cuadrantes de nivel distinto a 0; para evitar construir y obtener la generacion de cuadrantes iguales
	 * antes de su construccion se comprueba si existe un cuadrante con la misma estructura en la tabla de dispersion,
	 * en caso contrario, se crea dicho cuadrante y pasa a almacenarse en <tt>almacen</tt>
	 * 
	 * @param nwl, nel, swl, sel: subcuadrantes hijos del cuadrante que va a ser creado.
	 * @return la referencia al objeto <tt>Cuadrante</tt> compuesto por los 4 subcuadrantes 
	 */
	public static Cuadrante crear(Cuadrante nwl, Cuadrante nel, Cuadrante swl, Cuadrante sel){
		Cuadrante c;
		c = almacen.buscar(nwl,nel,swl,sel);
		if (c == null){
			c = new Cuadrante(nwl, nel, swl, sel);
			almacen.insertar(c);
		}
		return c;
	}
	
	/**
	 * Crea un cuadrante lleno de celdas vacias para un nivel dado.
	 * 
	 * @param nivel del c<tt>Cuadrante</tt> vacio a crear
	 * @return la referencia al objeto Cuadrante vacio creado
	 */
	public static Cuadrante crearVacio(int nivel){
		Cuadrante c;
		if (nivel==0){
			c = crear(0);
		}else{
			Cuadrante aux = crearVacio(nivel-1);
			c = crear(aux, aux, aux, aux);
		}
		return c;
	}
	
	/**
	 * Calcula la generacion del objeto <tt>Cuadrante</tt>, proporcional a su tama�o
	 * 
	 * @return El cuadrante resultado de calcular la generacion.
	 */
	public Cuadrante generacion(){
		if (this.res!=null){
			return this.res;
		}
		if (this.NIVEL==2){
			return this.generacion2();
		} else {
			Cuadrante n00, n01, n02, n10, n11, n12, n20, n21, n22;
			Cuadrante n = crear(this.getNW().getNE(), this.getNE().getNW(), this.getNW().getSE(), this.getNE().getSW());
			Cuadrante w = crear(this.getNW().getSW(), this.getNW().getSE(), this.getSW().getNW(), this.getSW().getNE());
			Cuadrante e = crear(this.getNE().getSW(), this.getNE().getSE(), this.getSE().getNW(), this.getSE().getNE());
			Cuadrante s = crear(this.getSW().getNE(), this.getSE().getNW(), this.getSW().getSE(), this.getSE().getSW());
			Cuadrante c = crear(this.getNW().getSE(), this.getNE().getSW(), this.getSW().getNE(), this.getSE().getNW());
			
			/*
			En caso de querer realizar el calculo de las iteraciones paso a paso, seria necesario sustituir la aplicacion 
			del metodo generacion() sobre los subcuadrantes, se aplicaria el metodo reducir()
			*/
			
			n00 = this.getNW().generacion(); 
			n01 = n.generacion();
			n02 = this.getNE().generacion();
			n10 = w.generacion();
			n11 = c.generacion();
			n12 = e.generacion();
			n20 = this.getSW().generacion();
			n21 = s.generacion();
			n22 = this.getSE().generacion();
			
			Cuadrante m00, m01, m10, m11;
			
			m00 = crear(n00, n01, n10, n11);
			m01 = crear(n01, n02, n11, n12);
			m10 = crear(n10, n11, n20, n21);
			m11 = crear(n11, n12, n21, n22);
			
			Cuadrante r00, r01, r10, r11;
			
			r00 = m00.generacion();
			r01 = m01.generacion();
			r10 = m10.generacion();
			r11 = m11.generacion();
			
			Cuadrante r = crear(r00, r01, r10, r11);
			
			this.res = r;
			return r;
		}
	}
	
	/**
	 * Calcula el resultado de la siguiente generacion de un objeto de tipo Cuadrante de nivel 2 tras aplicar las reglas
	 * del juego de la vida, 
	 * 
	 * @return el Cuadrante que representa la siguiente generacion
	 */
	public Cuadrante generacion2(){
		long vivas;
		Cuadrante c = null;
		Cuadrante[] res = new Cuadrante[4];
		
		int i = 0;
		for (int x = 1; x <= 2; x++){
			for (int y = 1; y <= 2; y++){
				vivas = this.getVecinas(x,y);
				if (this.getPixel(x,y).intValue()==1){
					if (!(vivas==2||vivas==3)) c = crear(0);
					else c = crear(1);
				} else {
					if (vivas==3) c = crear(1);
					else c = crear(0);
				}
				res[i] = c;
				i++;
			}
		}
		c = crear(res[0], res[2], res[1], res[3]);
		
		this.res = c;
		return c;
	}
	
	/**
	 * Comprueba las 8 celdas que rodean una concreta, para determinar cuantas estan vivas
	 * 
	 * @param x Coordenada horizaontal de la celda cuyas celdas colindantes van a ser comprobadas
	 * @param y Coordenada vertical de la celda
	 * @return el numero de celdas vivas que rodean al Cuadrante de nivel 0 (x,y)
	 */
	public int getVecinas(int x, int y){
		int vivas = 0;
		for (int i = -1; i <= 1; i++){
			for (int j = -1; j <= 1; j++){
				vivas+=this.getPixel(x+i, y+j).intValue();
			}
		}
		vivas-=this.getPixel(x, y).intValue();
		return vivas;
	}
	
	/**
	 * Dobla el tama�o del cuadrante, rellenando el espacio nuevo con cuadrantes vacios
	 * 
	 * @return <tt>Cuadrante</tt> 1 nivel superior al del objeto sobre el que se aplica, con su mismo contenido centrado
	 */
	public Cuadrante expandir(){
		Cuadrante c, c1;
		c1 = crearVacio(this.NIVEL-1);
		c = crear(crear(c1, c1, c1, this.getNW()),
					crear(c1, c1, this.getNE(), c1),
					crear(c1, this.getSW(), c1, c1),
					crear(this.getSE(), c1, c1, c1));
		return c;
	}
	
	/**
	 * Comprueba si existen celdas vivas en el borde del cuadrante, antes de expandir el mismo
	 * 
	 * @return <tt>Cuadrante</tt> 1 nivel superior al del objeto sobre el que se aplica, con su mismo contenido centrado
	 */
	public Cuadrante expandirSiNecesario(){
		Cuadrante c = this;
		while (c.hayVivasBorde()){
			c = c.expandir();
		}
		return c;
	}
	
	/**
	 * Comprueba que en la parte externa del cuadrante exista alguna celda viva
	 * 
	 * @return true en caso que existan celdas vivas, false en caso contrario.
	 */
	public boolean hayVivasBorde(){
		BigInteger pob = BigInteger.valueOf(0);
		
		pob = pob.add(this.getVivas());
		pob = pob.subtract(this.reducir().reducir().getVivas());
		if (pob.signum() == 1){
			return true;
		} else {
			return false;
		}
		
	}
	
	/**
	 * Reduce el Cuadrante a la parte central del mismo, descartando el resto del mismo.
	 * 
	 * @return el Cuadrante central con 1 nivel menor al del Cuadrante actual
	 */
	public Cuadrante reducir(){
		Cuadrante c, nw, ne, sw, se;
		nw = this.getNW().getSE();
		ne = this.getNE().getSW();
		sw = this.getSW().getNE();
		se = this.getSE().getNW();
		c = crear(nw, ne, sw, se);
		return c;
	}
	
	/**
	 * Metodo de acceso al nivel del objeto Cuadrante
	 * @return el nivel del objeto Cuadrante 
	 */
	public int getNivel(){
		return this.NIVEL;
	}
	/**
	 * @return el subcuadrante Noroeste
	 */
	public Cuadrante getNW() {
		return NW;
	}
	/**
	 * @return el subcuadrante Noreste
	 */
	public Cuadrante getNE() {
		return NE;
	}
	/**
	 * @return el subcuadrante Suroeste
	 */
	public Cuadrante getSW() {
		return SW;
	}
	/**
	 * @return el subcuadrante Sureste
	 */
	public Cuadrante getSE() {
		return SE;
	}
	
	/**
	 * @return el numero de celdas vivas del cuadrante
	 */
	public BigInteger getVivas(){
		return this.VIVAS;
	}
	
	/**
	 * Metodo para la obtencion del valor de la celda con coordenadas (x,y) sobre el <tt>Cuadrante</tt> 
	 * 
	 * @param x, y:  las coordenadas de le celda relativas al <tt>Cuadrante</tt> 
	 * @return el valor de la celda, 0 en caso de estar "muerta", 1 si esta "viva"
	 */
	public BigInteger getPixel(int x, int y){
		int mitad = (int) (Math.pow(2, this.getNivel())/2);
		if (this.NIVEL!=0){
			if (x<mitad) {
				if (y<mitad) {
					return this.getNW().getPixel(x, y);
				} else {
					return this.getSW().getPixel(x, y-mitad);
				}
			} else {
				if (y<mitad) {
					return this.getNE().getPixel(x-mitad, y);
				} else {
					return this.getSE().getPixel(x-mitad, y-mitad);
				}
			}
		} else {
			return this.VIVAS;
		}
	}
	
	/**
	 * Modifica el valor la celda (x,y) del Cuadrante. 
	 * 
	 * @param x,y las coordenadas del <tt>Cuadrante</tt> de nivel 0 a modificar
	 * @param v el valor inicial del cuadrante, 1 si esta viva, 0 en caso contrario
	 * @return la referencia al <tt>Cuadrante</tt> modificado.
	 */
	public Cuadrante setPixel(int x, int y, int v){
		int mitad = (int) Math.pow(2, this.getNivel())/2;
		Cuadrante nw = this.getNW();
		Cuadrante ne = this.getNE();
		Cuadrante sw = this.getSW();
		Cuadrante se = this.getSE();
		
		if (this.NIVEL!=0){
			if (x<mitad) {
				if (y<mitad) {
					nw = this.getNW().setPixel(x, y, v);
				} else {
					sw = this.getSW().setPixel(x, y-mitad, v);
				}
			} else {
				if (y<mitad) {
					ne = this.getNE().setPixel(x-mitad, y, v);
				} else {
					se = this.getSE().setPixel(x-mitad, y-mitad, v);
				}
			}
			return crear(nw, ne, sw, se);
		} else {
			return crear(v);
		}
	}
	
	/**
	 * Metodo de acceso al hash del objeto
	 * 
	 * @return el valor de la funcion de dispersion del <tt>Cuadrante</tt>
	 */
	public int getHashCode(){
		return this.HASHCODE;
	}
	
}
