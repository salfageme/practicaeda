/**
 * PRACTICA 2 - ESTRUCTURAS DE DATOS Y ALGORITMOS - Curso 2013/2014
 *
 * @author Daniel Barba Gutierrez (L1)
 * @author Samuel Alfageme Sainz (L1)
 */

/**
 * Clase que representa la tabla de dispersion cerrada que se usa como almacen de <tt>Cuadrantes</tt> 
 */
public class HashTable {
	
	private int m;
	private int n;
	private double maxL;
	
	private Cuadrante[] tabla;
	
	public HashTable(int m0, double maxL){
		this.maxL = maxL;
		this.m = m0;
		this.n = 0;
		this.tabla = new Cuadrante[this.m];
		for (int i = 0; i < this.m; i++){
			tabla[i] = null;
		}
	}
		
	private void reestructurar(){
		Cuadrante[] tmp = tabla;
		n = 0;
		m = 2*m;
		tabla = new Cuadrante[m];
		for (int i = 0; i < this.m; i++){
			this.tabla[i] = null;
		}
		for (int i = 0; i < tmp.length; i++){
			if (tmp[i] != null){
				insertar(tmp[i]);
			}
		}
	}
		
	public Cuadrante buscar(Cuadrante nw, Cuadrante ne, Cuadrante sw, Cuadrante se){
		int h = nw.getHashCode() + 11*ne.getHashCode() + 101*sw.getHashCode() + 1007*se.getHashCode();
		h = h ^ (h >>> 20) ^ (h >>> 12);
		h = h ^ (h >>> 7) ^ (h >>> 4);
		int i = h & (m-1);
		int s = h / m;
		if (s<0){s=-s;}
		if (s%2==0){s++;}
		
		while(tabla[i] != null && !(tabla[i].getNW().equals(nw)&&tabla[i].getNE().equals(ne)&&
									tabla[i].getSW().equals(sw)&&tabla[i].getSE().equals(se))){
			i = (i+s) % m;
		}
		return tabla[i];
	}
	
	public void insertar(Cuadrante c){
		this.n++;
		if ((1.0*n)/m > maxL){
			reestructurar();
		}
		int h = c.getNW().getHashCode() + 11*c.getNE().getHashCode() + 101*c.getSW().getHashCode() + 1007*c.getSE().getHashCode();
		h = h ^ (h >>> 20) ^ (h >>> 12);
		h = h ^ (h >>> 7) ^ (h >>> 4);
		int i = h & (m-1);
		int s = h / m;
		if (s<0){s=-s;}
		if (s%2==0){s++;}
		
		while(tabla[i] != null){
			i = (i+s) % m;
		}
		tabla[i] = c;
	}
}
