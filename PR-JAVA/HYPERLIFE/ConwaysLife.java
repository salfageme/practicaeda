/**
 * PRACTICA 2 - ESTRUCTURAS DE DATOS Y ALGORITMOS - Curso 2013/2014
 * 
 *  - HIPER VIDA - : optimizacion del automata celular del juego de la vida
 *  de Conway mediante el empleo de estructuras de datos avanzadas (QuadTrees),
 *  tecnicas de canonizacion mediante funciones hash y memorizacion de resultados
 *  de computo en tablas de dispersion cerradas. 
 * 
 * @author Daniel Barba Gutierrez (L1)
 * @author Samuel Alfageme Sainz (L1)
 */

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Scanner;


public class ConwaysLife {
	
	/**
	 * Rutina de ejecucion del automata: aplica, sobre un estado inicial dado, las reglas del "Juego de la Vida" de Conway
	 * tantas veces como iteraciones se especifiquen para obtener un resultado que puede ser almacenado en un nuevo fichero
	 * o simplemente resumido en unas lineas para comprobar su correccion y eficiencia.
	 * 
	 * Para el analisis de eficiencia del programa, se genera en la ruta en la que se ejecute un fichero .csv que contiene las 
	 * iteraciones del programa respecto al tiempo empleado en su consecucion, o respecto al numero de celulas vivas presentes en
	 * el Universo
	 * 
	 * @param args: en caso de no pasar ningun argumento a la hora de ejecutar el programa o que alguno de estos sea erroneo,
	 * se inicia una sesion interactiva para su recogida.
	 * 
	 * @param args[0]: String que contiene la ruta del fichero con el estado incial del automata.
	 * @param args[1]: Numero entero positivo, que representa el numero de iteraciones que se aplicaran sobre el e.i. del automata.
	 */
	public static void main(String[] args){
		
		Cuadrante root;
		BigInteger iter;
		String fileName;
		String fileResult = null;
		Scanner in = new Scanner(System.in);
		long time = 0;
	
		
		if(args.length==0){
		// Ejecucion interactiva
			System.out.print("Insertar la ruta del fichero patron: ");
			fileName = in.nextLine();
			iter = askIterNum(in);
		}else if(args.length==2){
		// Logica de ejecucion no interactiva
			fileName = args[0];
			try{
				iter = new BigInteger(args[1]);
			}catch(java.lang.NumberFormatException e){
				System.out.println("El numero de iteraciones especificado no es entero");
				iter = askIterNum(in);
			}
			if (iter.signum()==-1){
				System.out.println("Numero incorrecto de iteraciones");
				iter = askIterNum(in);
			}
		}else{
			System.out.println("Numero incorrecto de argumentos.");
			return;
		}
		
		root = readFile(fileName);
		
		// Generacion del archivo .csv 
		PrintWriter writer;
		try {
			writer = new PrintWriter(new FileWriter(fileName.split("\\.")[0] + ".csv")); 
			writer.println("Iteraciones,Tiempo,Cel.Vivas");
		} catch (IOException e) {
			System.out.println("El fichero de valores no se pudo crear.");
			return;
		}
		
		// Calculo del estado siguiente del automata.
		BigInteger c = BigInteger.ZERO;
		long t1,delta;
		while(c.compareTo(iter)<0){
			t1 = System.nanoTime();

			root = root.expandirSiNecesario();
			BigInteger aux = BigInteger.valueOf(2).pow(root.getNivel()-2);
			c = c.add(aux);
			root = root.generacion();
			
			delta = (System.nanoTime()-t1)/1000;
			time += delta;
			writer.println(c.toString() + "," + time/1000.0 + "," + root.getVivas().toString());
		}
		// Metodo para la obtencion del estado del automata graficamente, para niveles razonables
		if(root.getNivel()<13){
			try{
				GraphicUniverse matr = new GraphicUniverse(root);
				matr.saveResult();
			}catch(java.lang.OutOfMemoryError e){
				System.out.println("El resultado no pudo ser representado graficamente por su tama�o");
			}
		}
		writer.flush();
		writer.close();

		// Proceso de muestra de resultados:
		System.out.print("Introduzca la ruta del fichero destino: ");
		in = new Scanner(System.in);
		fileResult = in.nextLine();
		if(!fileResult.equals("")) writeFile(root, fileResult);
		else fileResult=null;
		in.close();
		writeInfo(root, fileName, fileResult, c, time);
		return;
	}
	
	/**
	 * Peticion del numero de iteraciones al usuario
	 * 
	 * @param in: objeto <tt>Scanner</tt> encargado de realizar la operacion de entrada 
	 * @postcondition: el numero introducido sera un entero positivo
	 * @return numero de iteraciones que se aplicaran sobre el estado inicial del automata
	 */
	public static BigInteger askIterNum(Scanner in){
		BigInteger i = null; 
		boolean iterCorrect = false;
		while (!iterCorrect){
			System.out.print("Especificar numero de iteraciones: ");
			while(!in.hasNextBigInteger()) {
				in.next();
				System.out.print("Introduzca un numero entero: ");
			}
			i = in.nextBigInteger();
			if (i.signum()==1) iterCorrect = true;
		}
		return i;
	}
	/**
	 * Lee el fichero que contiene el patron de partida para el automata y lo adecua a la estructura de un 
	 * Quadtree: situandolo centrado en el menor cuadrado con lado potencia de 2 que lo contiene.
	 * 
	 * @param fileName: ruta completa en disco del fichero que contiene el patron inicial
	 * @precondition las dimensiones especificadas en la primera linea del fichero deben ser correctas
	 * @return un objeto de tipo <tt>Cuadrante</tt> que contiene el patron que se expresa en el fichero 
	 */
	public static Cuadrante readFile(String fileName){
		Scanner reader = new Scanner(System.in);
		boolean fileOpen = false;
		Cuadrante c = null; 
		while (!fileOpen){
			try {
				reader = new Scanner(new FileReader(fileName));
				fileOpen = true;
				int filas = reader.nextInt();
				int columnas = reader.nextInt();
				int nivel=0;
				while (filas > Math.pow(2, nivel) || columnas > Math.pow(2, nivel)){
					nivel++;
				}
				int dim = (int) Math.pow(2, nivel);
				int f1 = (dim - filas)/2;
				int f2 = f1 + filas;
				int c1 = (dim -columnas)/2;
				int c2 = c1 + columnas;
				c = Cuadrante.crearVacio(nivel);
				for (int i=f1; i<f2; i++){
					String fila = reader.next();
					for (int j=c1; j<c2; j++){
						if (fila.charAt(j-c1)=='X')
							c = c.setPixel(i, j, 1);
					}
				}
				reader.close();
			} catch (FileNotFoundException e) {
				System.out.print("No se ha encontrado el fichero. Insertar nueva ruta: ");
				fileName = reader.nextLine();
			}
		}
		return c;
	}
	/**
	 * Guarda en el mismo formato que se emplea para la entrada, un <tt>Cuadrante</tt> dado: indicando sus dimensiones en las 2
	 * primeras lineas y, a continuacion, la representacion grafica del estado del automata.
	 * 
	 * @param root: objeto de tipo <tt>Cuadrante</tt> que representa un estado del automata que va a ser representado en un fichero 
	 * @param fileName: nombre del fichero de salida, en el que se representara el <tt>Cuadrante</tt> root
	 */
	public static void writeFile(Cuadrante root, String fileName){
		PrintWriter writer;
		try {
			writer = new PrintWriter(new FileWriter(fileName));
		} catch (IOException e) {
			System.out.println("El fichero no se pudo guardar");
			return;
		}
			int dim = (int) Math.pow(2, root.getNivel());
			writer.println(dim);
			writer.println(dim);
			for (int i=0; i<dim; i++){
				for (int j=0; j<dim; j++){
					if (root.getPixel(i, j).intValue()==1) writer.print('X');
					else writer.print('.');
				}
				writer.println();
			}
			writer.flush();
			writer.close();
	}
	
	/**
	 * Salida estandar de retroalimentacion del programa.
	 * 
	 * @param root: objeto de tipo <tt>Cuadrante</tt> que representa el estado del automata tras la ejecucion del programa
	 * @param fileOpen: ruta del fichero del que se leyo el patron inicial
	 * @param fileWritten: null en caso de no guardar los resultados, ruta del fichero en caso afirmativo
	 * @param iter: numero de iteraciones que se han aplicado al patron inicial para conseguir el estado descrito en <tt>root</tt>
	 * @param time: tiempo total empleado tras la realizacion de <tt>iter</tt> iteraciones sobre el patron contenido en <tt>fileOpen</tt>
	 */
	public static void writeInfo(Cuadrante root, String fileOpen, String fileWritten, BigInteger iter, long time){
		
		BigInteger dim = BigInteger.valueOf(2).pow(root.getNivel());
		System.out.println("Fichero origen: " + fileOpen);
		System.out.println("Numero de iteraciones: " + iter);
		System.out.println("Dimensiones finales: " + dim + "x" + dim);
		System.out.println("Celdas vivas: " + root.getVivas());
		System.out.println("Tiempo total empleado: " + time/1000.0 + " ms");
		if(fileWritten!=null) System.out.println("Fichero destino: " + fileWritten);
		
	}
}