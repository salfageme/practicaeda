/**
 * PRACTICA 2 - ESTRUCTURAS DE DATOS Y ALGORITMOS - Curso 2013/2014
 *
 * @author Daniel Barba Gutierrez (L1)
 * @author Samuel Alfageme Sainz (L1)
 */
import java.awt.*;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * Clase para realizar representaciones graficas de una matriz de datos que represente un <tt>Cuadrante</tt> raiz del 
 * automata del juego de la vida; orientada a visualizar el correcto funcionamiento de la practica.
 *
 */
public class GraphicUniverse extends JPanel {

  private final int WIDTH;
  private final int HEIGHT;
  private Cuadrante cuadr = null;

  public GraphicUniverse(Cuadrante root) {
	  this.cuadr = root;
	  int nivel = this.cuadr.getNivel();
	  this.WIDTH = (int) Math.pow(2,nivel);
	  this.HEIGHT = this.WIDTH;
	  setPreferredSize(new Dimension(this.WIDTH,this.HEIGHT));
	  JFrame f = new JFrame();
      f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      f.add(this, BorderLayout.CENTER);
      JScrollPane scrPane = new JScrollPane(this);
      f.add(scrPane);
      f.setPreferredSize(new Dimension(600, 600));
      f.pack();
      f.setTitle("HYPER-LIFE");
      f.setVisible(true);
  }

  public BufferedImage generateUniverseImg(){
	  final BufferedImage image;
	    int[] pix = { 0, 0, 0, 255 };

	    image = (BufferedImage) createImage(this.WIDTH, this.HEIGHT);
	    WritableRaster raster = image.getRaster();
	    for (int row = 0; row < this.HEIGHT; row++) {
	      for (int col = 0 ; col < this.WIDTH; col++) {
	    	int a = 0;
	    	if (this.cuadr.getPixel(row, col).intValue()==0) a=255;
	        pix[0] = a;
	        pix[1] = a;
	        pix[2] = a;
	        raster.setPixel(col, row, pix);
	      }
	    }
	    return image;
	  
  }
  @Override
  public void paintComponent(Graphics g) {
    BufferedImage image = generateUniverseImg();
    g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
  }
  
  /**
   * Metodo para almacenar el resultado grafico del automata en un fichero .jpg
   */
  public void saveResult(){
	  	BufferedImage image = generateUniverseImg();
	    try {
			ImageIO.write(image, "jpg", new File("Resultado.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
  	}
 }

