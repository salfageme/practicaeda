PR1 - ESTRUCTURAS DE DATOS Y ALGORITMOS
=======================================
El juego de la vida (Conway's Game of Life)
-------------------------------------------
## Descripción:
Entrega de PR1: Peso sobre la nota final de la asignatura: 15%
### Autoría:
El programa ha sido desarrollado para su entrega en grupo por:

* [Samuel Alfageme Sainz][samalfa]
* [Daniel Barba Gutiérrez][danbarb]

Pertenecientes al grupo de prácticas L1 de la asignatura [Estructuras de Datos y algoritmos][EDA] del [Grado en Ingeniería Informática][GRINI] de la Universidad de Valladolid.
## Entrada:
* Número de filas del patrón (n)
* Número de columnas (m)
* Le siguen n líneas de texto de m caracteres:
	* "." - Representan células muertas.
	* "X" - Identifican las células vivas.
## Salida: 
Se mostrará en la consola el patrón resultado de aplicar las reglas del Conway's game of life al universo que representa el fichero de texto durante el número de ejecuciones especificadas por el usuario; en la misma carpeta de la práctica se crea un fichero con extensión `.csv` y el mismo nombre que el fichero de texto de entrada (para el **análisis de tiempo de ejecución** frente al número de iteraciones)
## Ejecución:
```
python test.py
Especificar la ruta del fichero: exploder.txt
Numero de iteracciones sobre la matriz: 5

ESTADO INICIAL:

X.X.X
X...X
X...X
X...X
X.X.X

RESULTADO: 

..X.....X..
XX.XX.XX.XX
..X.....X..
```
## Licencia:
MIT a partir del momento de evaluación de la práctica (fecha por determinar)


[samalfa]: https://twitter.com/samu_alfageme
[danbarb]: https://twitter.com/corik87
[EDA]: http://www.infor.uva.es/~cvaca/asigs/eda.html
[GRINI]: https://www.inf.uva.es/