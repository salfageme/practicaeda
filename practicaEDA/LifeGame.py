# PRACTICA 1 - ESTRUCTURAS DE DATOS Y ALGORITMOS - 2-G-ING.INFORMATICA
# SAMUEL ALFAGEME SAINZ (L1)

class LifeGame():
	def __init__(self,board,iters):
		self.universe = board
		# PRE: All rows must have the same lenght for this to be true
		self.rows =len(board)
		self.columns = len(board[0])
		self.cells = None
		self.alive = None
		self.dead = None
		self.cellCount()
	def countLiveCells(self):
		return sum([row.count(True) for row in self.universe])
	def expandUniverse(self):
		self.universe = [[False]+foo+[False] for foo in self.universe]
		self.columns = len(self.universe[0])
		addition = [False]*self.columns
		self.universe =[addition]+self.universe+[addition]
		self.rows = len(self.universe)
		self.cellCount()
	def cellCount(self):
		self.cells = self.rows * self.columns
		self.alive = self.countLiveCells()
		self.dead = self.cells - self.alive
	def getUniverse(self):
		return self.universe
	def setUniverse(self,newUniverse):
		self.universe = newUniverse
		self.cellCount()
	def makeIter(self):
		print "nieeee"