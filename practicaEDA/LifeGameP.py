# PRACTICA 1 - ESTRUCTURAS DE DATOS Y ALGORITMOS - 2-G-ING.INFORMATICA
# SAMUEL ALFAGEME SAINZ (L1)
import copy
class LifeGame():
	def __init__(self,board,iters):
		self.universe = board
		# PRE: All rows must have the same lenght for this to be true
		self.rows =len(board)
		self.columns = len(board[0])
		self.cells = None
		self.alive = None
		self.dead = None
		self.cellCount()
	def countLiveCells(self):
		return sum([row.count(True) for row in self.universe])
	def expandUniverse(self):
		self.universe = [[False]+foo+[False] for foo in self.universe]
		self.columns = len(self.universe[0])
		addition = [False]*self.columns
		add2 = copy.deepcopy(addition) 				# HACER UNA DEEPCOPY MAS ELEGANTE (con addition[:] p.ej)
		self.universe =[addition]+self.universe+[add2]
		self.rows = len(self.universe)
		self.cellCount()
	def cellCount(self):
		self.cells = self.rows * self.columns
		self.alive = self.countLiveCells()
		self.dead = self.cells - self.alive
	def makeIter(self):
		newMat = copy.deepcopy(self.universe)
		for i, row in enumerate(self.universe):
			for j, column in enumerate(row):
				a,b,c,d = 1,1,1,1
				if i == 0: b = 0 
				elif i == self.rows-1: d = 0
				if j == 0: a = 0
				elif j == self.columns-1: c = 0
				surround = a*row[j-1]+c*row[c*(j+1)]+b*a*self.universe[i-1][j-1]+ \
				b*self.universe[i-1][j]+b*c*self.universe[i-1][b*c*(j+1)]+		\
				a*d*self.universe[a*d*(i+1)][j-1]+d*self.universe[d*(i+1)][j]+	\
				d*c*self.universe[c*d*(i+1)][d*c*(j+1)]
				if column:			# SI ESTA VIVA
					if surround != 2 and surround != 3:
						newMat[i][j] = False 
				else:				# SI ESTA MUERTA
					if surround == 3:
						newMat[i][j] = True
		self.setUniverse(newMat)

	def getUniverse(self):
		return self.universe
	def setUniverse(self,newUniverse):
		self.universe = newUniverse
		self.cellCount()
	def minimumUniverse(self):
		while sum(self.universe[0]) == 0:
			del self.universe[0]
		while sum(self.universe[-1]) == 0:
			del self.universe[-1]
		left = [l[0] for l in self.universe]
		while sum(left)==0:
			for line in self.universe:
				del line[0]
			left = [l[0] for l in self.universe]

		right = [m[-1] for m in self.universe]
		while sum(right)==0:
			for line in self.universe:
				del line[-1]
			right = [m[-1] for m in self.universe]
		self.cellCount()

#	a*b b b b b*c
#	a 			c
#	a 			c
#	a 			c
#	a*d d d d d*c

